let part = document.querySelector(".history");

function send_message() {
    let input = document.querySelector(".input_message")
    let scope = document.createElement("div")
    let scopebutton = document.createElement("div")
    
    let words = document.createElement("p")
    words.innerText = input.value
    
    let cleanbutton = document.createElement("button")
    cleanbutton.append("Limpar")
    cleanbutton.id="clean_button"
    cleanbutton.addEventListener("click", (e)=>{
            let i = document.querySelector(".new_message")
            i.remove()
        }
    )
    
    let editbutton = document.createElement("button")
    editbutton.classList.add("edit")
    editbutton.append("Editar")

    scopebutton.append(editbutton)
    scopebutton.append(cleanbutton)
    scopebutton.classList.add("scopebutton")
    
    scope.classList.add("new_message")
    scope.append(words)
    scope.append(scopebutton)

    part.append(scope);
}


let send_button = document.querySelector("#send_button")
send_button.addEventListener("click", ()=>{send_message()})